<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\VehiclesController;
use App\Http\Controllers\TurnsController;
use App\Http\Controllers\HistoricController;
use App\Http\Controllers\BrandController;
use App\Http\Controllers\ServicesController;
use App\Http\Controllers\ProcessesController;
use App\Http\Controllers\WorksController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/home');
});

Route::get('/home', [HomeController::class, 'getHome']);

// Protected routes
Route::group(['middleware' => 'auth'], function() {
    // Vehicles Routes
    Route::get('/vehicles', [VehiclesController::class, 'getVehicles']);
    Route::get('/vehicles/{client}', [VehiclesController::class, 'getClientVehicles']);
    Route::post('/vehicles', [VehiclesController::class, 'addVehicle']);
    Route::put('/vehicles/{plate}', [VehiclesController::class, 'updateVehicle']);
    Route::delete('/vehicles/{plate}', [VehiclesController::class, 'deleteVehicle']);
    // Turns Routes
    Route::get('/turns', [TurnsController::class, 'getTurns']);
    Route::post('/turns', [TurnsController::class, 'addTurn']);
    Route::put('/turns', [TurnsController::class, 'updateTurn']);
    Route::delete('/turns', [TurnsController::class, 'deleteTurn']);
    // Historic Routes
    Route::get('/historic', [HistoricController::class, 'getHistoric']);
    Route::get('/historic/processes/{code}', [ProcessesController::class, 'getProcesses']);
    // Brands search Route
    Route::get('/brands/{name}', [BrandController::class, 'getMatches']);
    // Get Services Route
    Route::get('/services/{name}', [ServicesController::class, 'getMatches']);
    // Processes Routes
    Route::post('/process', [ProcessesController::class, 'insertProcess']);
    // Works Routes
    Route::put('/works/{code}', [WorksController::class, 'finishWork']);
});

require __DIR__.'/auth.php';
