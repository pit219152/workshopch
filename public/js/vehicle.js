let tmpCard;
let tmpBrand;
let correctBrand = false;

const btnEditListener = () =>
    $(".btn-warning").on("click", (e) => {
        tmpCard = $(e.target).parent().siblings(".card-body");
        $("#editMileage").val(tmpCard.children(".mileageValue").text());
        const plate = tmpCard.children(".plateValue").text();
        $("#editPlate").text(plate);
    });
const btnDeleteListener = () =>
    $(".btnDelete").on("click", (e) => {
        tmpCard = $(e.target).parent().siblings(".card-body");
        const plate = tmpCard.children(".plateValue").text();
        $("#deletePlate").text(plate);
    });
btnEditListener();
btnDeleteListener();

$(".modal").on("hidden.bs.modal", () => {
    $("#formAdd").trigger("reset");
    $(".alert").addClass("visually-hidden").text("");
});

$("#addVehicleModal").on("show.bs.modal", () => {
    $("#brand").on("input", (e) => {
        correctBrand = false;
        const name = $(e.target).val().toUpperCase();
        if (name !== "" && name.length > 3) {
            $("#brandList").html("");
            $.ajax({
                type: "GET",
                url: `/brands/${name}`,
                dataType: "json",
                success: (data) => {
                    console.log(data);
                    let items = "";
                    $.each(data, (index, item) => {
                        items += `<div class='class list-group-item list-group-item-action brand-item' brandCode='${item.code}'>${item.name}</div>`;
                    });
                    $("#brandList").html(items);
                },
            });
        } else $("#brandList").html("");
    });
    $(document).on("click", ".brand-item", (e) => {
        $("#brandList").html("");
        const code = $(e.target).attr("brandCode");
        const name = $(e.target).text();
        $("#brand").val(name);
        tmpBrand = code;
        correctBrand = true;
    });
});

$("#btnAddSend").on("click", () => {
    if ($("#formAdd")[0].checkValidity()) {
        if (!correctBrand) {
            return $("#msgAdd")
                .removeClass("visually-hidden")
                .addClass("alert-danger")
                .text("Error! Modelo incorrecto");
        }
        const _token = $("input[name='_token'").val();
        const plate = $("input[name='plate'").val();
        const mileage = $("input[id='addMileage'").val();
        $.ajax({
            type: "POST",
            url: "/vehicles",
            data: {
                _token: _token,
                plate: plate,
                mileage: mileage,
                model: tmpBrand,
            },
        })
            .done((response, status) => {
                console.log(response);
                $("#msgAdd")
                    .removeClass("visually-hidden")
                    .addClass("alert-success")
                    .text("Vehiculo agregado");
                // add Card manually
                $("#vehicles").append(`
                    <div class="card col-lg-3 col-md-5 col-10 m-2">
                        <div class="card-img-top text-center"><i class="fas fa-car" style="font-size: 6rem;"></i></div>
                        <div class="card-body">
                            <h5 class="plate">Placa:</h5> <span class="plateValue">${response.plate}</span> 
                            <h6 class="mt-2">Cliente:</h6> ${response.owner}
                            <h6 class="mt-2 ">Kilometraje:</h6> <span class="mileageValue">${response.mileage}</span> Km.
                            <h6 class="mt-2">Marca:</h6> ${response.model}
                            <h6 class="mt-2">Modelo:</h6> ${response.brand}
                        </div>
                        <div class="card-footer">
                            <button type="button" class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#editVehicleModal"><i class="fas fa-edit"></i> Editar</button>
                            <button type="button" class="btn btn-danger btnDelete" data-bs-toggle="modal" data-bs-target="#deleteVehicleModal"><i class="fas fa-trash"></i> Eliminar</button>
                        </div>
                    </div>
                `);
                setTimeout(() => {
                    // bind buttons listeners to new Card
                    btnEditListener();
                    btnDeleteListener();
                    $("#btnAddClose").trigger("click");
                }, 2000);
            })
            .fail((jqXHR) => console.log(jqXHR));
    } else $("#formAdd")[0].reportValidity();
});

$("#btnEditSend").on("click", () => {
    if ($("#formEdit")[0].checkValidity()) {
        const _token = $("input[name='_token'").val();
        const plate = tmpCard.children(".plateValue").text();
        const mileage = $("input[id='editMileage'").val();
        if (Number(mileage) > Number(tmpCard.children(".mileageValue").text()))
            $.ajax({
                type: "PUT",
                url: `/vehicles/${plate}`,
                data: { _token: _token, _method: "PUT", mileage: mileage },
            })
                .done((response, status) => {
                    console.log(response);
                    tmpCard.children(".mileageValue").text(mileage);
                    setTimeout(() => $("#btnEditClose").trigger("click"), 1000);
                })
                .fail((jqXHR) => console.log(jqXHR));
        else {
            $("#msgEdit")
                .removeClass("visually-hidden")
                .addClass("alert-danger")
                .text("Error! El valor ingresado es menor al valor actual");
        }
    } else $("#formEdit")[0].reportValidity();
});

$("#btnDeleteSend").on("click", () => {
    const _token = $("input[name='_token'").val();
    const plate = tmpCard.children(".plateValue").text();
    $.ajax({
        type: "DELETE",
        url: `/vehicles/${plate}`,
        data: { _token: _token },
    })
        .done((response, status) => {
            console.log(response);
            $("#msgDelete")
                .removeClass("visually-hidden")
                .addClass("alert-warning")
                .text("Vehiculo eliminado");
            tmpCard.parent().remove(); // Remove card
            setTimeout(() => $("#btnDeleteClose").trigger("click"), 2000);
        })
        .fail((jqXHR) => console.log(jqXHR));
});
