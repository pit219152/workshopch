var codeTurn;

const table = $("#agenda_table").DataTable({
    language: {
        url: "/assets/DataTables/Spanish.json",
    },
    initComplete: () => {
        btncontainer.appendTo("#tableBtns");
    },
});
new $.fn.dataTable.Buttons(table, {
    buttons: [
        {
            className: "",
            extend: "copyHtml5",
            text:
                "<button class='btn btn-info'><i class='fas fa-clone'></i></button>",
            titleAttr: "Copy",
            exportOptions: { columns: [0, 1, 2] },
        },
        {
            extend: "excelHtml5",
            text:
                "<button class='btn btn-success'><i class='fas fa-file-excel'></i></button>",
            titleAttr: "Excel",
            exportOptions: { columns: [0, 1, 2] },
        },
        {
            extend: "pdfHtml5",
            text:
                "<button class='btn btn-danger'><i class='fas fa-file-pdf'></i></button>",
            titleAttr: "PDF",
            exportOptions: { columns: [0, 1, 2] },
        },
    ],
});
const btncontainer = table.buttons().container();

$(".btnaccept").on("click", (e) => {
    $("#acceptClient").html($(e.target).parents("td").attr("turnclient"));
    $.ajax({
        type: "GET",
        url: "/vehicles/" + $(e.target).parents("td").attr("turnid"),
        success: mountProcess,
        error: handleError,
    });

    codeTurn = $(e.target).parents("td").attr("id");
});

$("#btnAcceptSend").on("click", () => {
           
    const _token = $("input[name='_token'").val();
    var aTurnVehicle = $("#vehicles").val();;

    console.log(codeTurn);
    
    $.ajax({
        type: "PUT",
        url: "/turns",
        data: {
            _token: _token,
            turn: codeTurn,
            vehicle: aTurnVehicle
        },
    })
        .done((response, status) => {
            console.log(response);
            $("#msgAccept")
                .removeClass("visually-hidden")
                .addClass("alert-success")
                .text("Turno aceptado");
            setTimeout(() => $("#btnAcceptClose").trigger("click"), 2000);

            $("#status"+codeTurn).html("EXPIRADO");
            $("#"+codeTurn).html("El turno ya está aceptado");
        })
        .fail((jqXHR) => console.log(jqXHR));
    
});


function mountProcess(data) {
    console.log(data);

    $("#vehicles").html("");

    
    data.forEach((vehicle) => {
        var op = document.createElement("option");
        
        op.value = vehicle[0];
        op.innerHTML = vehicle[1];

        $("#vehicles").append(op);
    });
    
}

function handleError(_jqXHR, _textStatus, error) {
    console.log(error);
}