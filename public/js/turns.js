document.addEventListener("DOMContentLoaded", (event) => {
    const message = document.getElementById("message");
    const subbtn = document.getElementById("subbtn");

    valbtn = document.getElementById("validatebtn");

    if (valbtn) {
        valbtn.addEventListener("click", validatedate);
    }

    function validatedate() {
        subbtn.disabled = true;
        // INPUT DATE
        idate = document.getElementById("date");
        // INPUT TIME
        itime = document.getElementById("time");

        if (itime.value == "" || idate.value == "")
            message.innerHTML = "No se admiten campos vacíos";

        var idateYMD = idate.value.split("-");

        var itimeHM = itime.value.split(":");

        let cDate = new Date();
        let cdd = cDate.getDate();
        let cmm = cDate.getMonth() + 1;
        let cyy = cDate.getFullYear();

        if (idateYMD[0] < cyy || idateYMD[1] < cmm || idateYMD[2] <= cdd)
            message.innerHTML = "La fecha ingresada no es valida";
        else {
            cDate.setFullYear(idateYMD[0]);
            cDate.setMonth(idateYMD[1]);
            cDate.setDate(idateYMD[2]);
            if (
                cDate.getDay() == 0 ||
                (cDate.getDay() == 6 && (itimeHM[0] < 8 || itimeHM[0] > 12)) ||
                itimeHM[0] < 8 ||
                itimeHM[0] > 16
            )
                message.innerHTML = "La fecha ingresada no es valida";
            else {
                message.innerHTML =
                    "La fecha ingresada es valida, presione <b>Agendar turno</b> para guardar su turno.";
                subbtn.disabled = false;
            }
        }
    }

    $("#subbtn").on("click", () => {
        const form = $("#turnForm")[0];
        const formData = new FormData(form);
        const _token = $("input[name='_token'").val();

        $.ajax({
            type: "POST",
            url: "/turns",
            data: {
                _token: _token,
                date: formData.get("date"),
                time: formData.get("time"),
            },
        })
            .done(() => (window.location.href = "/turns"))
            .fail((jqXHR) => console.log(jqXHR));
    });

    $("#cancelbtn").on("click", () => {
        const _token = $("input[name='_token'").val();

        $("#cancelModal").modal("show");
    });

    $("#btnCancelSend").on("click", () => {
        const _token = $("input[name='_token'").val();

        $.ajax({
            type: "DELETE",
            url: `/turns`,
            data: { _token: _token },
        })
            .done((response, status) => {
                console.log(response);
                $("#messagge")
                    .addClass("alert-warning")
                    .text("Turno cancelado");
                $("#cancelbtn").attr("disabled", true);

                setTimeout(() => (window.location.href = "/turns"), 2000);
            })
            .fail((jqXHR) => console.log(jqXHR));

        $("#cancelModal").modal("hide");
    });
});
