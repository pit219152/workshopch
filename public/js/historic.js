let tmpWork;
let tmpService;
let correctService;

const table = $("#historic_table").DataTable({
    language: {
        url: "/assets/DataTables/Spanish.json",
    },
    initComplete: () => {
        btncontainer.appendTo("#tableBtns");
    },
});
new $.fn.dataTable.Buttons(table, {
    buttons: [
        {
            className: "",
            extend: "copyHtml5",
            text:
                "<button class='btn btn-info'><i class='fas fa-clone'></i></button>",
            titleAttr: "Copy",
            exportOptions: { columns: [0, 1, 2] },
        },
        {
            extend: "excelHtml5",
            text:
                "<button class='btn btn-success'><i class='fas fa-file-excel'></i></button>",
            titleAttr: "Excel",
            exportOptions: { columns: [0, 1, 2] },
        },
        {
            extend: "pdfHtml5",
            text:
                "<button class='btn btn-danger'><i class='fas fa-file-pdf'></i></button>",
            titleAttr: "PDF",
            exportOptions: { columns: [0, 1, 2] },
        },
    ],
});
const btncontainer = table.buttons().container();

$(".btnpro").on("click", (e) => {
    $.ajax({
        type: "GET",
        url: "/historic/processes/" + $(e.target).parents("td").attr("workid"),
        success: mountProcess,
        error: handleError,
    });
});
$(".btnAddPro, .btnFinWork").on(
    "click",
    (e) => (tmpWork = $(e.target).parents("td").attr("workid"))
);

$("#addProcessModal").on("show.bs.modal", () => {
    $("#service").on("input", (e) => {
        correctService = false;
        const name = $(e.target).val().toUpperCase();
        if (name !== "" && name.length > 3) {
            $("#serviceList").html("");
            $.ajax({
                type: "GET",
                url: `/services/${name}`,
                dataType: "json",
                success: (data) => {
                    console.log(data);
                    let items = "";
                    $.each(data, (index, item) => {
                        items += `<div class='class list-group-item list-group-item-action service-item' serviceCode='${item.code}'>${item.name}</div>`;
                    });
                    $("#serviceList").html(items);
                },
            });
        } else $("#serviceList").html("");
    });
    $(document).on("click", ".service-item", (e) => {
        $("#serviceList").html("");
        const code = $(e.target).attr("serviceCode");
        const name = $(e.target).text();
        $("#service").val(name);
        tmpService = code;
        correctService = true;
    });
});

$(".modal").on("hidden.bs.modal", () => {
    $("#formAddPro").trigger("reset");
    $(".alert").addClass("visually-hidden").text("");
});

$("#btnProSend").on("click", () => {
    if ($("#formAddPro")[0].checkValidity()) {
        if (!correctService) {
            return $("#msgAdd")
                .removeClass("visually-hidden")
                .addClass("alert-danger")
                .text("Error! Servicio incorrecto");
        }
        const _token = $("input[name='_token'").val();
        const charge = $("input[id='charge'").val();
        $.ajax({
            type: "POST",
            url: "/process",
            data: {
                _token: _token,
                work: tmpWork,
                service: tmpService,
                charge: charge,
            },
        })
            .done((response, status) => {
                console.log(response);
                $("#msgAdd")
                    .removeClass("visually-hidden")
                    .addClass("alert-success")
                    .text("Proceso agregado");
                setTimeout(() => $("#btnAddProClose").trigger("click"), 2000);
            })
            .fail((jqXHR) => console.log(jqXHR));
    }
});

$("#btnFinishSend").on("click", () => {
    const _token = $("input[name='_token'").val();
    $.ajax({
        type: "PUT",
        url: `/works/${tmpWork}`,
        data: {
            _token: _token,
        },
    })
        .done((response, status) => {
            console.log(response);
            $("#msgFinish")
                .removeClass("visually-hidden")
                .addClass("alert-success")
                .text("Trabajo entregado");
            const workRow = $(`td[workid=${tmpWork}]`);
            workRow.find(".btnAddPro, .btnFinWork").remove();
            workRow
                .siblings("td[type='delivery_date']")
                .text(response.delivery_date);
            setTimeout(() => $("#btnFinClose").trigger("click"), 2000);
        })
        .fail((jqXHR) => console.log(jqXHR));
});

function mountProcess(data) {
    console.log(data);

    $("#modalbody").html("");

    data.forEach((proc) => {
        var row = document.createElement("p");
        var service = document.createElement("p");
        var charge = document.createElement("p");

        row.className = "row";
        service.className = "col";
        charge.className = "col";

        service.innerHTML = proc[0];
        charge.innerHTML = proc[1];

        row.append(service, charge);
        $("#modalbody").append(row);
    });
}

function handleError(_jqXHR, _textStatus, error) {
    console.log(error);
}
