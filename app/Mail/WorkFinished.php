<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Work;
use App\Models\Process;

class WorkFinished extends Mailable
{
    use Queueable, SerializesModels;

    public $work;
    public $processes;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Work $work)
    {
        $this->work = $work;
        $processes = Process::where('work', $work->code)->get();
        $processesArray = array();
        foreach($processes as $process) array_push($processesArray, array($process->getService->name, $process->charge));
        $this->processes = $processesArray;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.workFinished');
    }
}
