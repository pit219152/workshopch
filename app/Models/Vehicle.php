<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Client;
use App\Models\Brand;

class Vehicle extends Model
{
    protected $primaryKey = 'plate';
    public $incrementing = false;
    protected $keyType = 'string';

    use HasFactory;

    public function getOwner() {
        return $this->belongsTo(Client::class, 'owner', 'id');
    }

    public function getBrand() {
        return $this->belongsTo(Brand::class, 'brand', 'code');
    }
}
