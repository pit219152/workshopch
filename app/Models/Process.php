<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Service;

class Process extends Model
{
    use HasFactory;

    public function getService()
    {
        return $this->belongsTo(Service::class, 'service', 'code');
    }
}
