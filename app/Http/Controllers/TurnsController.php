<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Turn;
use App\Models\Client;
use App\Models\Work;

class TurnsController extends Controller
{
    public function getTurns() {
        $user = Auth::user();

        if ($user->type == 'C'){
            $client = Client::where('user_id', '=', $user->id)->first();
            $turn = Turn::where('client', '=', $client->id)->where('status','V')->first();
            return view('turns', array('user' => $user, 'turn' => $turn));
        }else{
            $turns = Turn::all();
            
            return view('agenda', array('user' => $user, 'turns' => $turns));
        }

        
    }

    public function addTurn(Request $request) {
        $user = Auth::user();
        $parDate = ''.$request->date.' '.$request->time;
        $lastTurn = Turn::latest('code')->first();
        $lastCode = ((int)$lastTurn->code)+1;

        $turn = new Turn();
        $turn->code = strval($lastCode);
        $turn->client = Client::where('user_id', '=', $user->id)->firstOrFail()->id;
        $turn->date = $parDate;
        $turn->status = 'V';
        $turn->save();

        return view('turns', array('user' => $user, 'turn' => $turn));
    }
    
    public function deleteTurn() {
        $user = Auth::user();
        $client = Client::where('user_id', '=', $user->id)->firstOrFail()->id;

        $turn = Turn::where('client', $client)->where('status', 'V');
        $turn->delete();

        return response()->json(['message' => 'DELETED']);
    }

    public function updateTurn(Request $request) {
        
        $turn = Turn::where('code', $request->turn)->where('status', 'V')->firstOrFail();
        
        $turn->status = 'E';
        $turn->save();
        
        $lastWork = Work::latest('code')->first();
        $lastCode = ((int)$lastWork->code)+1;
        
        $work = new Work();
        $work->code = $lastCode;
        $work->vehicle = $request->vehicle;
        $work->start_date = now();
        $work->save();
        
        return response()->json(['message' => 'UPDATED']);
    }
    
}
