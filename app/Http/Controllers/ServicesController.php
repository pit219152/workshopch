<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Service;

class ServicesController extends Controller
{
    public function getMatches($name) {
        return response()->json(Service::where('name', 'like', '%'.$name.'%')->get());
    }
}
