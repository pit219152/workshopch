<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Process;
use App\Models\Service;
use App\Models\Employee;

class ProcessesController extends Controller
{
    public function getProcesses($code) {
        $processes = Process::where('work', $code)->get();
        $sData = array();
        foreach($processes as $process) array_push($sData, array($process->getService->name, $process->charge));
        return $sData;
    }

    public function insertProcess(Request $request) {
        $user = Auth::user();
        $lastProcess = Process::select('code')->orderBy('code','desc')->first();
        $process = new Process();
        $process->code = strval(intval($lastProcess->code)+1);
        $process->employee = Employee::where('user_id', '=', $user->id)->firstOrFail()->id;
        $process->service = $request->service;
        $process->work = $request->work;
        $process->charge = $request->charge;
        $process->save();
        return response()->json(['message' => 'INSERTED PROCESS']);
    }
}
