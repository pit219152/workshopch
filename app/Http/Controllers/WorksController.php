<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Models\Work;
use App\Models\User;
use App\Models\Client;
use App\Models\Vehicle;
use App\Mail\WorkFinished;

class WorksController extends Controller
{
    public function finishWork($code) {
        $work = Work::where('code', '=', $code)->firstOrFail();
        $work->delivery_date = date('Y-m-d H:i:s');
        $work->save();
        $client = Vehicle::where('plate', '=', $work->vehicle)->first()->getOwner->user_id;
        $user = User::findOrFail($client);
        Mail::to($user->email)->send(new WorkFinished($work));
        return response()->json(['message' => 'FINISHED: '.$code, 'delivery_date' => $work->delivery_date]);
    }
}
