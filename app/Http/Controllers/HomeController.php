<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Service;

class HomeController extends Controller
{
    public function getHome() {
        $user = Auth::user();
        $services = Service::all();

        return view('home', array('user' => $user, 'services' => $services));
    }
}
