<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Work;
use App\Models\Vehicle;
use App\Models\Client;

class HistoricController extends Controller
{
    public function getHistoric() {
        $user = Auth::user();
        if ($user->type == 'E') $works = Work::all();
        else if($user->type == 'C') {
            $client = Client::where('user_id', '=', $user->id)->first();
            $vehicles = Vehicle::where('owner', '=', $client->id)->get();
            $plates = array();
            foreach($vehicles as $vehicle) array_push($plates, $vehicle->plate);
            $works = Work::all();
            $worksArray = array();
            foreach($works as $work) if(in_array($work->vehicle, $plates)) array_push($worksArray, $work);
            $works = $worksArray;
        }

        return view('historic', array('user' => $user, 'works' => $works));
    }
}
