<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Client;
use App\Models\Vehicle;

class VehiclesController extends Controller
{
    public function getVehicles() {
        $user = Auth::user();
        if ($user->type == 'E') $vehicles = Vehicle::all();
        else if ($user->type == 'C') $vehicles = Vehicle::where('owner', '=', Client::where('user_id', '=', $user->id)->firstOrFail()->id)->get();

        return view('vehicles', array('user' => $user, 'vehicles' => $vehicles));
    }

    public function addVehicle(Request $request) {
        $user = Auth::user();
        $vehicle = new Vehicle();
        $vehicle->plate = $request->plate;
        $vehicle->owner = Client::where('user_id', '=', $user->id)->firstOrFail()->id;
        $vehicle->brand = $request->model;
        $vehicle->mileage = $request->mileage;
        $vehicle->save();
        return response()->json([
            'plate' => $vehicle->plate, 
            'owner' => $vehicle->getOwner->name, 
            'model' => $vehicle->getBrand->name, 
            'mileage' => $vehicle->mileage, 
            'brand' => $vehicle->getBrand->getDivision->name
        ]);
    }

    public function updateVehicle(Request $request, $plate) {
        $vehicle = Vehicle::where('plate', '=', $plate)->firstOrFail();
        $requestValue = (int)$request->mileage;
        $vehicleValue = (int)$vehicle->mileage;
        if ($requestValue > $vehicleValue) {
            $vehicle->mileage = $request->mileage;
            $vehicle->save();
            return response()->json(['message' => 'EDITED']);
        }
        return response()->json(['message' => 'Kilometraje debe ser mayor al actual', 'data' => var_dump($request->input())], 400);   
    }

    public function deleteVehicle(Request $request, $plate) {
        $vehicle = Vehicle::where('plate', '=', $plate)->firstOrFail();
        $vehicle->delete();
        return response()->json(['message' => 'DELETED '.$plate]);
    }

    public function getClientVehicles($client) {
        
        $vehicles = Vehicle::where('owner', '=', $client)->get();
        $vehiclesArray = array();

        foreach($vehicles as $vehicle) array_push($vehiclesArray, array($vehicle->plate, $vehicle->plate.' '.$vehicle->getBrand->name));

        return $vehiclesArray;
    }

}
