<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Brand;

class BrandController extends Controller
{
    public function getMatches($name) {
        return response()->json(Brand::where('name', 'like', '%'.$name.'%')->get());
    }
}
