@extends('layouts.master')

@section('content')
    <!-- Modal Add -->
    <div class="modal fade" id="addVehicleModal" tabindex="-1" role="dialog" aria-labelledby="addVehicleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title" id="addVehicleModalLabel">Agregar Vehiculo</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form id="formAdd" method="POST">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col">
                                <label class="form-label" for="plate">Placa:</label>
                                <input class="form-control col-4" type="text" name="plate" id="plate" minlength=6 maxlength=6 pattern="[A-Z]{3}[0-9]{3}" placeholder="ABC123" required>
                            </div>
                            <div class="col">
                                <label class="form-label" for="addMileage">Kilometraje:</label>
                                <input class="form-control" type="number" name="mileage" id="addMileage" min="0" max="999999" required>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col">
                                <label class="form-label" for="brand">Modelo:</label>
                                <input class="form-control" type="text" placeholder="Escriba el modelo de su vehículo" name="brand" id="brand" required>
                            </div>
                        </div>
                    </form>
                    <div class="autocomplete list-group" id="brandList" style="position: relative; max-height: 150px; overflow-y: scroll;"></div>
                    <div class="alert visually-hidden mt-3" id="msgAdd"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" id="btnAddClose">Cerrar</button>
                    <button type="button" class="btn btn-primary" id="btnAddSend">Agregar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Edit -->
    <div class="modal fade" id="editVehicleModal" tabindex="-1" role="dialog" aria-labelledby="editVehicleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title" id="editVehicleModalLabel">Editar Vehiculo <span id="editPlate"></span></h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form id="formEdit" method="POST">
                        {{ method_field('PUT') }}
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-6 col-lg-4">
                                <label class="form-label" for="editMileage">Kilometraje nuevo:</label>
                                <input class="form-control" type="number" name="mileage" id="editMileage" required> 
                            </div>
                        </div>
                    </form>
                    <div class="alert visually-hidden mt-3" id="msgEdit"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" id="btnEditClose">Cerrar</button>
                    <button type="button" class="btn btn-primary" id="btnEditSend">Guardar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Delete -->
    <div class="modal fade" id="deleteVehicleModal" tabindex="-1" role="dialog" aria-labelledby="deleteVehicleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title" id="deleteVehicleModalLabel">Eliminar Vehiculo <span id="deletePlate"></span></h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form id="formDelete" method="POST">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <h3>¿Está seguro de eliminar el vehículo?</h3>
                    </form>
                    <div class="alert visually-hidden mt-3" id="msgDelete"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" id="btnDeleteClose">Cerrar</button>
                    <button type="button" class="btn btn-danger" id="btnDeleteSend">Eliminar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row mt-3">
            <div class="col">
                <button type="button" class="btn btn-primary btn-lg" data-bs-toggle="modal" data-bs-target="#addVehicleModal"><h3><i class="fas fa-plus"></i> Agregar Vehículo</h3> </button>
            </div>
        </div>
        <div class="row justify-content-center mt-2" id="vehicles">
            <h1>Vehículos: </h1>
            @foreach ($vehicles as $vehicle)
                <div class="card col-lg-3 col-md-5 col-10 m-2">
                    <div class="card-img-top text-center"><i class="fas fa-car" style="font-size: 6rem;"></i></div>
                    <div class="card-body">
                        <h5 class="plate">Placa:</h5> <span class="plateValue">{{$vehicle->plate}}</span> 
                        <h6 class="mt-2">Cliente:</h6> {{$vehicle->getOwner->name}}
                        <h6 class="mt-2 ">Kilometraje:</h6> <span class="mileageValue">{{$vehicle->mileage}}</span> Km.
                        <h6 class="mt-2">Marca:</h6> {{$vehicle->getBrand->getDivision->name}}
                        <h6 class="mt-2">Modelo:</h6> {{$vehicle->getBrand->name}}
                    </div>
                    <div class="card-footer">
                        <button type="button" class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#editVehicleModal"><i class="fas fa-edit"></i> Editar</button>
                        <button type="button" class="btn btn-danger btnDelete" data-bs-toggle="modal" data-bs-target="#deleteVehicleModal"><i class="fas fa-trash"></i> Eliminar</button>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

    <script src="/js/vehicle.js"></script>
@stop
