@extends('layouts.master')

@section('content')
    @if (!$user)
    <div class="container">
      <div class="row align-items-center">
        <div class="col-9 fs-1" >
          ¿Aún no tienes tu cuenta? ¡Regístrate YA!
        </div>
        <a href="/register" class="btn btn-primary btn-lg btn-block col-3">
          REGISTRARME &nbsp;<i class="fas fa-user-plus"></i>
        </a>
      </div>
    </div>
    <hr>
    @endif

    <!--CAROUSEL-->
    <div class="container">
        <br>
        <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                <img src="{{ url('/assets/images/WS_CAROUSEL1.png') }}" class="d-block w-100" height="480" height="440" alt="uno">
                </div>
                <div class="carousel-item">
                <img src="{{ url('/assets/images/WS_CAROUSEL2.png') }}" class="d-block w-100" height="480" alt="dos">
                </div>
                <div class="carousel-item">
                <img src="{{ url('/assets/images/WS_CAROUSEL3.png') }}" class="d-block w-100" height="480" alt="tres">
                </div>
                <div class="carousel-item">
                <img src="{{ url('/assets/images/WS_CAROUSEL4.png') }}" class="d-block w-100" height="480" alt="tres">
                </div>
            </div>
    
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </a>
        </div>
    </div>
    <br><br>

    <!--ACCORDION-->

    <div class="container">

        <h3>Nuestros servicios</h3>
        <div class="accordion" id="accordionServices">
          @foreach( $services as $service )
            <div class="accordion-item">
              <h2 class="accordion-header" id="heading{{ $service->code }}">
                <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapse{{ $service->code }}" aria-expanded="false" aria-controls="collapse{{ $service->code }}">
                  {{ $service->name }}
                </button>
              </h2>
              <div id="collapse{{ $service->code }}" class="accordion-collapse collapse show" aria-labelledby="heading{{ $service->code }}" data-bs-parent="#accordionServices">
                <div class="accordion-body">
                  {{ $service->description }}
                </div>
              </div>
            </div>
          @endforeach
        </div>
    </div>
    <br><br>


    <div class="container lightgrey">
        <h3>Contáctenos</h3>
        <div class="row">
            <div class="col">
                <b>EMAIL:</b> tallerch@gmail.com
            </div>
            <div class="col">
                <b>TELÉFONO Y CELULAR:</b> 7313663 - (+57) 3146104970
            </div>
        </div><br>


        <div class="row">
          <div class="col">
              <b>HORARIO DE ATENCIÓN:</b>
              <p>
                Lunes - Viernes : 8:00 am A 12:00 pm ; 1:30 pm A 6:00 pm.
              </p>
              <p>
                Sábados  y Festivos : 8:00 am A 2:00 pm.
              </p>
          </div>
        </div>

        <br>
        <p>
            <b>Visítenos en Facebook</b>
            <br>
            <a href="https://www.facebook.com/Tallerchicaiza/">
                <img src="{{ url('/assets/images/WS_FACEBOOK.png') }}" height="50px" alt="Facebook">
            </a>
        </p>

        <p>
            <b>Ubíquenos</b>
        </p>
        <div class="row">
            <iframe class= "col md-12"src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d997.2251615303358!2d-77.2887624708377!3d1.228891840740526!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e2ed387dda2353b%3A0xf63e105c7b855030!2sCl.%2018a%20%2343-2%20a%2043-22%2C%20Pasto%2C%20Nari%C3%B1o!5e0!3m2!1ses!2sco!4v1611715715081!5m2!1ses!2sco" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        </div>
        <br>
    </div>
@stop