<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>TallerCh</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ url('/assets/bootstrap/css/bootstrap.min.css') }}">
    <!-- DataTables CSS -->
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/DataTables/datatables.css') }}">
    <!-- Local CSS -->
    <link href="{{ url('/assets/css/styles.css') }}" rel="stylesheet">
    <!-- FontAwesome Icons -->
    <script src="https://kit.fontawesome.com/1742c5ad30.js" crossorigin="anonymous"></script>
    <!-- DataTables -->
    <script src="{{ url('/assets/DataTables/jquery.js') }}" ></script>
    <!-- Bootstrap JavaScript -->
    <script src="{{ url('/assets/bootstrap/js/bootstrap.min.js') }}" ></script>
    <script src="{{ url('/assets/bootstrap/js/bootstrap.bundle.min.js') }}" ></script>
</head>
<body>

    <!--NAVBAR-->
    @include('partials.navbar')

    <div>
        @yield('content')
    </div>

</body>
</html>