<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Su vehículo está listo</title>
</head>
<body>
    <p>Hola! le informamos que su vehículo de placas: <strong>{{ $work->vehicle }}</strong> está listo</p>
    <p>Estos son los datos de los procedimientos realizados:</p>
    <ul>
        @foreach( $processes as $process )
            <li><strong>Servicio:</strong> {{ $process[0] }}. Valor: ${{$process[1]}}</li>
        @endforeach
    </ul>
</body>
</html>