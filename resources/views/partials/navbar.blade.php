
<nav class="navbar navbar-light bg-light">
    <div class="container">    
        <a class="navbar-brand px-2" href="/home">
            <img  src="{{ url('/assets/images/WS_logo_nav_140.png') }}" width="140" height="40" alt="LOGO_WS_140">
        </a>

        @if ($user)

            @if ($user->type == 'C')
                <a href="/vehicles" class="nav-link navitem">Vehículos</a>
                <a href="/turns" class="nav-link navitem">Turnos</a>
                <a href="/historic" class="nav-link navitem">Historial</a>
            @else
                <a href="/turns" class="nav-link navitem">Agenda</a>
                <a href="/historic" class="nav-link navitem">Trabajos</a>
                <!-- <a href="/services" class="nav-link navitem">Servicios</a> -->
            @endif
            

            <form action="{{ url('/logout') }}" method="POST" style="display:inline">
                {{ csrf_field() }}
                <button type="submit" class="btn btn-link nav-link navitem" style="display:inline;cursor:pointer">
                    Cerrar sesión <i class="fas fa-sign-out-alt"></i>
                </button>
            </form>
        @else
            <a href="{{ url('/login') }}" class="nav-link navitem">Iniciar Sesión <i class="fas fa-sign-in-alt"></i></a>            
        @endif
    </div>
</nav>

