@extends('layouts.master')

@section('content')
    <link rel="stylesheet" href="{{ url('/assets/DataTables/datatables.min.css') }}">
    <!-- Modal -->
    <div class="modal fade" id="acceptModal" tabindex="-1" role="dialog" aria-labelledby="acceptModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title" id="acceptModalLabel">Aceptar turno</span></h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form id="formAccept" method="POST">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <p>
                            <b>Cliente: </b><span id="acceptClient"></span>
                        </p>
                        <p>
                            Seleccione el vehículo con el que desea crear el trabajo activo para el cliente. 
                        </p>
                        <div class="row">
                            <div class="col-6 col-lg-4">
                                <label class="form-label" for="editMileage">Vehículo:</label>
                                <select name="vehicles" id="vehicles"></select>
                            </div>
                        </div>
                    </form>
                    <div class="alert visually-hidden mt-3" id="msgAccept"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" id="btnAcceptClose">Cerrar</button>
                    <button type="button" class="btn btn-primary" id="btnAcceptSend">Aceptar turno</button>
                </div>
            </div>
        </div>
    </div>

    <br>
    <div class="container mb-2">
        <div class="row justify-content-center">
            <div class="btn-group">
                <div id="tableBtns"></div>
            </div>
        </div>
        <div class="row justify-content-center mt-2">
            <div class="col">
                <table id="agenda_table" class="table table-striped table-bordered table-hover table-responsive" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>Cliente</th>
                            <th>Fecha de turno</th>
                            <th>Estatus</th>
                            <th>Aceptar turno</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach( $turns as $turn )
                            <tr>
                                <td >{{ $turn->getClient->name }}</td>
                                <td>{{ $turn->date }}</td>

                                @if( $turn->status == 'E')
                                    <td id="status{{ $turn->code}}">Expirado</td>
                                    <td >El turno ya está aceptado</td>
                                @else
                                    <td id="status{{ $turn->code}}">Vigente</td>
                                    <td id= "{{ $turn->code}}" turnclient="{{ $turn->getClient->name }}" turnid="{{$turn->getClient->id}}" style="text-align: center;">
                                        <button class="btn btn-success btn-lg btnaccept" data-bs-toggle="modal" data-bs-target="#acceptModal"><i class="fas fa-clipboard-check"></i></button>  
                                    </td>
                                @endif

                                
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        
    </div>

    <!--DataTables Scripts-->
    <script src="{{ url('/assets/DataTables/datatables.min.js') }}" ></script>
    <script src="{{ url('/assets/DataTables/Buttons-1.6.5/js/buttons.bootstrap4.min.js') }}" ></script>
    <script src="{{ url('/js/agenda.js') }}" ></script>
    
@stop
