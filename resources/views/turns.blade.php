@extends('layouts.master')

@section('content')
    
        
        @if ($turn)
            <div class="container lightgrey">
            <h1>Turnos</h1>
                <div>
                    <h3>
                        ¡Ya tiene un turno!
                    </h3>
                    <p>
                        Su turno esta agendado para <b>{{$turn->date}}</b>.
                    </p>
                    
                    <p>
                        Procura ser puntual.
                    </p>
                    <br>
                    <div id="messagge">
                    </div>
                
                    <button id="cancelbtn" class="btn btn-danger">Cancelar turno</button>
                    <br>
                    <br>
                </div>
            </div>

            <!-- Modal CANCEL -->
            <div class="modal fade" id="cancelModal" tabindex="-1" role="dialog" aria-labelledby="cancelModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h1 class="modal-title" id="cancelModalLabel">Cancelar turno</span></h1>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                            <div class="modal-body">

                            ¿Está seguro de cancelar su turno?
                            
                            </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" id="btnCancelClose">Cerrar</button>
                            <button type="button" class="btn btn-danger" id="btnCancelSend">Cancelar Turno</button>
                        </div>
                    </div>
                </div>
            </div>

        @else
            <div class="container lightgrey">
                <h1>Turnos</h1>
                <div>
                    <h3>
                        ¿Quiere solicitar un turno?
                    </h3>
                    <p>
                        Para agendar tu turno simplemente selecciona una fecha dentro de nuestro horario de trabajo y presiona <b>"Validar turno"</b>, si la fecha es valida presiona <b>"Agendar turno"</b> y ¡listo!
                    </p>
                    <br>
                </div>
                <div>
                    <form id="turnForm" method="POST">
                        {{ csrf_field() }}
                        <div class="row justify-content-center">
                            <div class="col-3">
                                <label for="date">Fecha</label>
                                <input type="date" name="date" id="date">
                            </div>
                            <div class="col-3">
                                <label for="time">Hora</label>
                                <input type="time" name="time" id="time">
                            </div>
                        </div>
                        <br>
                        <div class="row justify-content-center">
                            <div id="message" class="col-6 text-center"></div>
                        </div>
                    </form>
                    <div class="row justify-content-center mb-3">
                        <div class="col-8 btn-group rounded-pill" role="group" aria-label="Rounded pill example">
                            <button id="validatebtn" type="button" class="btn btn-secondary btn-lg">Validar turno</button>
                            <input id="subbtn" type="submit" value="Agendar turno" class="btn btn-primary btn-lg btn-arrow" disabled>
                        </div>
                    </div>
                    <br>
                </div>
            </div>
        @endif
    

    <script src="{{ url('/js/turns.js') }}" ></script>
@stop
