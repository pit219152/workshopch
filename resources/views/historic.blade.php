@extends('layouts.master')

@section('content')
    <link rel="stylesheet" href="{{ url('/assets/DataTables/datatables.min.css') }}">
    <!-- Modal -->
    <div class="modal fade" id="processModal" tabindex="-1" role="dialog" aria-labelledby="procModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="procModalLabel">Procedimientos</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <div >
                <p>
                    Procedimientos realizados al vehículo.
                </p>
                <div class="row">
                    <div class="col"><b>Servicio</b></div><div class="col"><b>Costo</b></div>
                </div>
            </div>
            <div id="modalbody">
                ...
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
    </div>
    @if ($user->type == 'E')
        <!-- Modal Add proccess to work -->
        <div class="modal fade" id="addProcessModal" tabindex="-1" role="dialog" aria-labelledby="addProcModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="addProcModalLabel">Agregar Procedimiento</h3>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <h5>Agregar procedimiento al trabajo del vehículo.</h5>
                <form id="formAddPro">
                    {{ csrf_field() }}
                    <div class="row mt-2">
                        <div class="col">
                            <label class="form-label" for="service">Servicio:</label>
                            <input class="form-control" type="text" placeholder="Escriba el nombre del servicio" name="service" id="service" required>
                            <div class="autocomplete list-group" id="serviceList" style="position: relative; max-height: 150px; overflow-y: scroll;"></div>
                        </div>
                        <div class="col">
                            <label class="form-label" for="charge">Costo:</label>
                            <input class="form-control" type="number" name="charge" id="charge" min="0" max="999999" required>
                        </div>
                    </div>
                </form>
                <div class="alert visually-hidden mt-3" id="msgAdd"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" id="btnAddProClose">Cerrar</button>
                <button type="button" class="btn btn-primary" id="btnProSend">Agregar</button>
            </div>
            </div>
        </div>
        </div>
        <!-- Modal Finish Work -->
        <div class="modal fade" id="finishWorkModal" tabindex="-1" role="dialog" aria-labelledby="finishWorkModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title" id="finishWorkModalLabel">¿Terminar trabajo?</h1>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form id="formFinish" method="POST">
                            {{ method_field('PUT') }}
                            {{ csrf_field() }}
                            <h3>¿Está seguro de terminar el trabajo del vehículo?</h3>
                        </form>
                        <div class="alert visually-hidden mt-3" id="msgFinish"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" id="btnFinClose">Cerrar</button>
                        <button type="button" class="btn btn-success" id="btnFinishSend">Terminar</button>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <br>
    <div class="container mb-2">
        <div class="row justify-content-center">
            <div class="btn-group">
                <div id="tableBtns"></div>
            </div>
        </div>
        <div class="row justify-content-center mt-2">
            <div class="col">
                <table id="historic_table" class="table table-striped table-bordered table-hover table-responsive" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>Vehículo</th>
                            <th>Fecha de ingreso</th>
                            <th>Fecha de egreso</th>
                            <th>Procedimientos</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach( $works as $work )
                            <tr>
                                <td >{{ $work->vehicle }}</td>
                                <td>{{ $work->start_date }}</td>
                                <td type="delivery_date">{{ $work->delivery_date }}</td>
                                
                                <td workid="{{$work->code}}" style="text-align: center;">
                                    <button class="btn btn-primary btn-lg btnpro" data-bs-toggle="modal" data-bs-target="#processModal"><i class="fas fa-info"></i></button>  
                                    @if ($user->type == 'E' && $work->delivery_date == null)
                                        <button class="btn btn-warning btn-lg btnAddPro" data-bs-toggle="modal" data-bs-target="#addProcessModal"><i class="fas fa-plus-circle"></i></button>  
                                        <button class="btn btn-success btn-lg btnFinWork" data-bs-toggle="modal" data-bs-target="#finishWorkModal"><i class="fas fa-check-circle"></i></button>  
                                    @endif
                                </td>
                                
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        
    </div>

    <!--DataTables Scripts-->
    <script src="{{ url('/assets/DataTables/datatables.min.js') }}" ></script>
    <script src="{{ url('/assets/DataTables/Buttons-1.6.5/js/buttons.bootstrap4.min.js') }}" ></script>
    <script src="{{ url('/js/historic.js') }}" ></script>
    
@stop
