<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('processes', function (Blueprint $table) {
            $table->string('code');
            $table->string('employee');
            $table->string('service');
            $table->string('work');
            $table->integer('charge');
            $table->timestamps();

            $table->primary('code');
            $table->foreign('employee')->references('id')->on('employees');
            $table->foreign('service')->references('code')->on('services');
            $table->foreign('work')->references('code')->on('works');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('processes');
    }
}
