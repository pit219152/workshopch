<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */

    private $usersArray = array(
        
        array(
            "name" => "GEORJE NOGUERA",
            "email" => "GEORJE1NOGUERA@gmail.com",
            "type" => "E",
            "password" => "micontraseña123"
        ),
        array(
            "name" => "MICHAEL TUTALCHA",
            "email" => "MICHAEL2TUTALCHA@gmail.com",
            "type" => "E",
            "password" => "micontraseña123"
        ),
        array(
            "name" => "ALBERTH TUTALCHA",
            "email" => "ALBERTH3TUTALCHA@gmail.com",
            "type" => "E",
            "password" => "micontraseña123"
        ),
        array(
            "name" => "YOMERO CHICAIZA",
            "email" => "YOMERO4CHICAIZA@gmail.com",
            "type" => "E",
            "password" => "micontraseña123"
        ),
        array(
            "name" => "CARLOS RAMIREZ",
            "email" => "CARLOS5RAMIREZ@gmail.com",
            "type" => "E",
            "password" => "micontraseña123"
        ),
        array(
            "name" => "ALEXANDER PAZ",
            "email" => "ALEXANDER6PAZ@gmail.com",
            "type" => "E",
            "password" => "micontraseña123"
        ),
        array(
            "name" => "PAOLA CORTEZ",
            "email" => "PAOLA7CORTEZ@gmail.com",
            "type" => "E",
            "password" => "micontraseña123"
        ),
        array(
            "name" => "NATHALIA PACHAJOA",
            "email" => "NATHALIA8PACHAJOA@gmail.com",
            "type" => "E",
            "password" => "micontraseña123"
        ),
        array(
            "name" => "YOLANDA ACOSTA",
            "email" => "YOLANDA9ACOSTA@gmail.com",
            "type" => "E",
            "password" => "micontraseña123"
        ),
        array(
            "name" => "EDWIN TUTALCHA",
            "email" => "EDWIN10TUTALCHA@gmail.com",
            "type" => "E",
            "password" => "micontraseña123"
        ),
        array(
            "name" => "YOMERO JAVIER PEREZ ERAZO",
            "email" => "YOMERO76PEREZ@GMAIL.COM",
            "type" => "C",
            "password" => "micontraseña123"
        ),
        array(
            "name" => "TEODORA ARTEAGA BURGOS",
            "email" => "TEODORA55BURGOS@GMAIL.COM",
            "type" => "C",
            "password" => "micontraseña123"
        ),
        array(
            "name" => "JUAN ALBERTO MARRERO MENA",
            "email" => "JUAN37MARRERO@GMAIL.COM",
            "type" => "C",
            "password" => "micontraseña123"
        ),
        array(
            "name" => "MARGARITA ACOSTA SANTA CRUZ",
            "email" => "MARGARITA49SANTA@GMAIL.COM",
            "type" => "C",
            "password" => "micontraseña123"
        ),
        array(
            "name" => "ANDER HOLGADO PAZ",
            "email" => "ANDER74PAZ@GMAIL.COM",
            "type" => "C",
            "password" => "micontraseña123"
        ),
        array(
            "name" => "DANA GABRIELA MONTILLA DIAZ",
            "email" => "DANA28MONTILLA@GMAIL.COM",
            "type" => "C",
            "password" => "micontraseña123"
        ),
        array(
            "name" => "JUAN LUIS MARCOS GUERRA",
            "email" => "JUAN78MARCOS@GMAIL.COM",
            "type" => "C",
            "password" => "micontraseña123"
        ),
        array(
            "name" => "MARIA JOSE PRADO PRADO",
            "email" => "MARIA4PRADO@GMAIL.COM",
            "type" => "C",
            "password" => "micontraseña123"
        ),
        array(
            "name" => "ANTONIO JOSE RUIZ LAZO",
            "email" => "ANTONIO98RUIZ@GMAIL.COM",
            "type" => "C",
            "password" => "micontraseña123"
        ),
        array(
            "name" => "IRIA NORA GONZALES CHAMORRO",
            "email" => "IRIA69GONZALES@GMAIL.COM",
            "type" => "C",
            "password" => "micontraseña123"
        ),
        array(
            "name" => "DANIEL FERNANDO PACHAJOA MORA",
            "email" => "DANIEL52PACHAJOA@GMAIL.COM",
            "type" => "C",
            "password" => "micontraseña123"
        ),
        array(
            "name" => "LUISA ESTEFANIA SOSA GUTIERRES",
            "email" => "LUISA48SOSA@GMAIL.COM",
            "type" => "C",
            "password" => "micontraseña123"
        ),
        array(
            "name" => "GUSTAVO ANDRES CATALA MENDEZ",
            "email" => "GUSTAVO56CATALA@GMAIL.COM",
            "type" => "C",
            "password" => "micontraseña123"
        ),
        array(
            "name" => "ROSA MARIANA ANGULO PEREZ",
            "email" => "ROSA13ANGULO@GMAIL.COM",
            "type" => "C",
            "password" => "micontraseña123"
        ),
        array(
            "name" => "LUIS ALEJANDRO BURBANO NUÑEZ",
            "email" => "LUIS60BURBANO@GMAIL.COM",
            "type" => "C",
            "password" => "micontraseña123"
        ),
        array(
            "name" => "SIMONA CABRERA BASTIDAS",
            "email" => "SIMONA42BASTIDAS@GMAIL.COM",
            "type" => "C",
            "password" => "micontraseña123"
        ),
        array(
            "name" => "CARLOS ANDRES SALAZAR NOGUERA",
            "email" => "CARLOS523SALAZAR@GMAIL.COM",
            "type" => "C",
            "password" => "micontraseña123"
        ),
        array(
            "name" => "DIANA LIZETH PAZ SERÓN",
            "email" => "DIANA99PAZ@GMAIL.COM",
            "type" => "C",
            "password" => "micontraseña123"
        ),
        array(
            "name" => "CAMILO ALBERTO REALPE CALPA",
            "email" => "CAMILO00REALPE@GMAIL.COM",
            "type" => "C",
            "password" => "micontraseña123"
        ),
        array(
            "name" => "ANA CECILIA NAVIA OROZCO",
            "email" => "CECILIANA23@GMAIL.COM",
            "type" => "C",
            "password" => "micontraseña123"
        ),
        array(
            "name" => "NELSON ROSAS CHALPUD",
            "email" => "NEROCHA@GMAIL.COM",
            "type" => "C",
            "password" => "micontraseña123"
        )
    );

    public function run()
    {
        // \App\Models\User::factory(10)->create();
        self::seedUsers();
        $this->command->info('Tabla usuarios inicializada con datos!');
    }

    public function seedUsers(){
        DB::table('users')->delete();
        foreach( $this->usersArray as $user ) {
            $seedUser = new User;
            $seedUser->name = $user['name'];
            $seedUser->email = $user['email'];
            $seedUser->type = $user['type'];
            $seedUser->password = bcrypt($user['password']);

            $seedUser->save();
        }
    }

    
}
